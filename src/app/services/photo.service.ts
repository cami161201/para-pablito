import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class PhotoService {
  info: any = {};
  cargada: boolean = false;
  equipo: any[] = [];

  constructor(public http: HttpClient) {
    this.carga();
  }

  public carga() {
    this.http.get('https://amanecer-594e9-default-rtdb.firebaseio.com/Equipo.json')
      .subscribe((resp:any) => {
        this.equipo=resp;
        console.log(resp);
        
      });
  }
}
